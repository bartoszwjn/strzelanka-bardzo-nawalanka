// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUD.h"
#include "PlayerCharacter.h"
#include "Engine/Canvas.h"


// Primary draw call for the HUD.
void APlayerHUD::DrawHUD() {
  APlayerCharacter* Character = Cast<APlayerCharacter>(GetOwningPawn());
  if (Character) {
    FVector2D HealthBarDrawPosition(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.9f);
    float HP = Character->CurrentHitPoints;
    FLinearColor HealthDrawColor = FLinearColor::Yellow;
    if (HP == Character->HitPoints) {
      HealthDrawColor = FLinearColor::Green;
    } else if (HP <= Character->HitPoints * 0.25f) {
      HealthDrawColor = FLinearColor::Red;
    }
    FCanvasTextItem HealthInfo(HealthBarDrawPosition, FText::AsNumber(Character->CurrentHitPoints), Font, HealthDrawColor);
    Canvas->DrawItem(HealthInfo);
  }
}