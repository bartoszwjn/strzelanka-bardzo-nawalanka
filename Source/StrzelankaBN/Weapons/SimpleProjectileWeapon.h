// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponBase.h"
#include "SimpleProjectileWeapon.generated.h"

UCLASS()
class STRZELANKABN_API ASimpleProjectileWeapon : public AWeaponBase {
  GENERATED_BODY()

public:
  // Sets default values for this actor's properties
  ASimpleProjectileWeapon();

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

public:
  // Called every frame
  virtual void Tick(float DeltaTime) override;

  // Gun's root component
  UPROPERTY(EditAnywhere)
  class USceneComponent* BaseComponent;

  // The gun's mesh
  UPROPERTY(EditAnywhere, Category="Mesh")
  class UStaticMeshComponent* GunMesh;

  // Gun muzzle's offset from the character's loacation
  UPROPERTY(EditAnywhere, Category="Gameplay")
  FVector MuzzleOffset;

  // Projectile class to spawn
  UPROPERTY(EditDefaultsOnly, Category="Projectile")
  TSubclassOf<class ASimpleProjectile> ProjectileClass;

  // Damage dealt by the weapon
  UPROPERTY(EditAnywhere, Category="Damage")
  float WeaponDamage;

  // Called when player presses the Primary Fire button
  UFUNCTION()
  virtual void OnStartPrimaryFire() override;

  // Called when player equips the weapon
  UFUNCTION()
  virtual void OnEquip() override;
};
