// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"


// Sets default values
AWeaponBase::AWeaponBase() {
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay() {
  Super::BeginPlay();
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);
}

// Called when player presses the Primary Fire button
void AWeaponBase::OnStartPrimaryFire() {
}

// Called when player releases the Primary Fire button
void AWeaponBase::OnStopPrimaryFire() {
}

// Called when player presses the Secondary Fire button
void AWeaponBase::OnStartSecondaryFire() {
}

// Called when player releases the Secondary Fire button
void AWeaponBase::OnStopSecondaryFire() {
}

// Called when player equips the weapon
void AWeaponBase::OnEquip() {
}

// Called when player unequips the weapon
void AWeaponBase::OnUnequip() {
}
