// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleProjectileWeapon.h"
#include "Projectiles/SimpleProjectile.h"


// Sets default values
ASimpleProjectileWeapon::ASimpleProjectileWeapon() {
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  BaseComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
  BaseComponent->SetMobility(EComponentMobility::Movable);
  RootComponent = BaseComponent;

  MuzzleOffset = FVector(50.0f, 10.0f, -10.0f);

  ProjectileClass = ASimpleProjectile::StaticClass();

  WeaponDamage = 15.0f;
}

// Called when the game starts or when spawned
void ASimpleProjectileWeapon::BeginPlay() {
  Super::BeginPlay();
}

// Called every frame
void ASimpleProjectileWeapon::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);
}

// Called when player presses the Primary Fire button
void ASimpleProjectileWeapon::OnStartPrimaryFire() {
  if (ProjectileClass != nullptr) {
    UWorld* const World = GetWorld();
    if (World) {
      const FRotator SpawnRotation = GetActorRotation();
      const FVector SpawnLocation = GetActorLocation() + SpawnRotation.RotateVector(MuzzleOffset);

      FActorSpawnParameters ProjectileSpawnParameters;
      ProjectileSpawnParameters.SpawnCollisionHandlingOverride =
        ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
      ProjectileSpawnParameters.Instigator = Instigator;
      ProjectileSpawnParameters.Owner = this;

      ASimpleProjectile* Projectile =
        World->SpawnActor<ASimpleProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ProjectileSpawnParameters);

      if (Projectile) {
        Projectile->ProjectileDamage = WeaponDamage;
      }
    }
  }
}

  // Called when player equips the weapon
  void ASimpleProjectileWeapon::OnEquip() {
    // Print debug message
    if (GEngine) {
      GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("Player equipped SimpleProjectileWeapon."));
    }
  }
