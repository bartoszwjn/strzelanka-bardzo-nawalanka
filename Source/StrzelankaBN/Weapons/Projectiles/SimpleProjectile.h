// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SimpleProjectile.generated.h"

UCLASS()
class STRZELANKABN_API ASimpleProjectile : public AActor {
  GENERATED_BODY()

public:
  // Sets default values for this actor's properties
  ASimpleProjectile();

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

public:
  // Called every frame
  virtual void Tick(float DeltaTime) override;

  // Sphere collision component
  UPROPERTY(VisibleDefaultsOnly)
  class USphereComponent* CollisionComponent;

  // Visible component
  UPROPERTY(EditDefaultsOnly, Category="Visual")
  class UStaticMeshComponent* VisibleComponent;

  // Projectile movement component
  UPROPERTY(VisibleAnywhere)
  class UProjectileMovementComponent* ProjectileMovementComponent;

  // Damage dealt by the projectile
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
  float ProjectileDamage;

  // Called when projectile hits something
  UFUNCTION()
  void OnHit(
    UPrimitiveComponent* HitComponent,
    AActor* OtherActor,
    UPrimitiveComponent* OtherComponent,
    FVector NormalImpulse,
    const FHitResult& Hit
  );
};
