// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"


// Sets default values
ASimpleProjectile::ASimpleProjectile() {
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  // Setup collision component
  CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
  CollisionComponent->InitSphereRadius(0.5f);
  CollisionComponent->BodyInstance.SetCollisionProfileName("Projectile");
  CollisionComponent->OnComponentHit.AddDynamic(this, &ASimpleProjectile::OnHit);

  // Players can't walk on it
  CollisionComponent->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
  CollisionComponent->CanCharacterStepUpOn = ECB_No;

  RootComponent = CollisionComponent;

  VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
  VisibleComponent->SetupAttachment(CollisionComponent);

  ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
  ProjectileMovementComponent->UpdatedComponent = CollisionComponent;
  ProjectileMovementComponent->InitialSpeed = 10000.0f;
  ProjectileMovementComponent->MaxSpeed = 10000.0f;
  ProjectileMovementComponent->bRotationFollowsVelocity = true;

  InitialLifeSpan = 4.0f;
}

// Called when the game starts or when spawned
void ASimpleProjectile::BeginPlay() {
  Super::BeginPlay();
}

// Called every frame
void ASimpleProjectile::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);
}

// Called when projectile hits something
void ASimpleProjectile::OnHit(
  UPrimitiveComponent* HitComponent,
  AActor* OtherActor,
  UPrimitiveComponent* OtherComponent,
  FVector NormalImpulse,
  const FHitResult& Hit
) {
  if (
    (OtherActor != nullptr) && (OtherActor != this) &&
    (OtherActor != GetOwner()) && (OtherActor != GetOwner()->GetOwner())
  ) {
    FVector HitDirection = GetOwner()->GetActorLocation() - OtherActor->GetActorLocation();
    HitDirection.Normalize(1.0f);
    OtherActor->TakeDamage(
      ProjectileDamage,
      FPointDamageEvent(
        ProjectileDamage,
        Hit,
        HitDirection,
        UDamageType::StaticClass()
      ),
      Instigator->GetController(),
      this
    );
  }
  Destroy();
}