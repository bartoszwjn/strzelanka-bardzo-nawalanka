// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponBase.generated.h"

UCLASS()
class STRZELANKABN_API AWeaponBase : public AActor {
  GENERATED_BODY()

public:
  // Sets default values for this actor's properties
  AWeaponBase();

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

public:
  // Called every frame
  virtual void Tick(float DeltaTime) override;


  // Called when player presses the Primary Fire button
  UFUNCTION()
  virtual void OnStartPrimaryFire();

  // Called when player releases the Primary Fire button
  UFUNCTION()
  virtual void OnStopPrimaryFire();

  // Called when player presses the Secondary Fire button
  UFUNCTION()
  virtual void OnStartSecondaryFire();

  // Called when player releases the Secondary Fire button
  UFUNCTION()
  virtual void OnStopSecondaryFire();

  // Called when player equips the weapon
  UFUNCTION()
  virtual void OnEquip();

  // Called when player unequips the weapon
  UFUNCTION()
  virtual void OnUnequip();
};
