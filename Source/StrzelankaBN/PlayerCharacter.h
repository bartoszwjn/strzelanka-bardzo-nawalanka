// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class STRZELANKABN_API APlayerCharacter : public ACharacter {
  GENERATED_BODY()

public:
  // Sets default values for this character's properties
  APlayerCharacter();

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

public:
  // Called every frame
  virtual void Tick(float DeltaTime) override;

  // Called to bind functionality to input
  virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


  // Camera used when controlling the character
  UPROPERTY(VisibleAnywhere)
  class UCameraComponent* PlayerViewCameraComponent;


  // Functions used for controlling the character
  UFUNCTION()
  void MoveForward(float Value);

  UFUNCTION()
  void MoveRight(float Value);

  UFUNCTION()
  void StartJump();

  UFUNCTION()
  void StopJump();


  // Default player weapon class
  UPROPERTY(EditAnywhere, Category="Weapon")
  TSubclassOf<class AWeaponBase> DefaultWeaponClass;

  // Weapon that the player is using at the moment
  UPROPERTY(VisibleAnywhere, Category="Weapon")
  class AWeaponBase* EquippedWeapon;


  // Functions used for firing a weapon
  UFUNCTION()
  void StartPrimaryFire();

  UFUNCTION()
  void StopPrimaryFire();

  UFUNCTION()
  void StartSecondaryFire();

  UFUNCTION()
  void StopSecondaryFire();


  // Maxumim Hit Points
  UPROPERTY(EditAnywhere, Category="Hit Points")
  float HitPoints;

  // Current Hit Points
  float CurrentHitPoints;

  // How long in seconds does it take to start regenerating Hit Points after receiving damage
  UPROPERTY(EditAnywhere, Category="Hit Points")
  float TimeToRegenerateHitPoints;

  // How many Hit Points are regenerated per second
  UPROPERTY(EditAnywhere, Category="Hit Points")
  float HitPointsRegenerationRate;

  // Are hit points regenerating at this moment?
  bool bIsRegeneratingHitPoints;

  // A handle for the Hit Points regeneration timer
  FTimerHandle HitPointsRegenerationTimerHandle;


  // Start regenerating Hit Points
  UFUNCTION()
  void StartRegeneratingHitPoints();

  // Stop regenerating Hit Points
  UFUNCTION()
  void StopRegeneratingHitPoints();


  // Take damage
  UFUNCTION()
  virtual float TakeDamage(
    float DamageAmount,
    const struct FDamageEvent& DamageEvent,
    AController* EventInstigator,
    AActor* DamageCauser
  );


  // Called when player's Hit Points reach 0
  // Instigator is the Controller responsible for killing the character
  // DeathCauser is the Actor responsible for killing the character
  UFUNCTION()
  void Die(AController* Instigtor, AActor* DeathCauser);
};
