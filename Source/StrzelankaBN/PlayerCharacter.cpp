// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Weapons/WeaponBase.h"
#include "Weapons/SimpleProjectileWeapon.h"


// Sets default values
APlayerCharacter::APlayerCharacter() {
  // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  // Setup camera view
  PlayerViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerViewCameraComponent"));
  PlayerViewCameraComponent->SetupAttachment(GetCapsuleComponent());
  PlayerViewCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, BaseEyeHeight));
  PlayerViewCameraComponent->bUsePawnControlRotation = true;

  // Setup default weapon
  DefaultWeaponClass = ASimpleProjectileWeapon::StaticClass();

  // Set Hit Points
  HitPoints = 100.0f;
  TimeToRegenerateHitPoints = 3.0f;
  HitPointsRegenerationRate = 25.0f;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay() {
  Super::BeginPlay();

  // Setup Hit Points and regeneration
  CurrentHitPoints = HitPoints;
  bIsRegeneratingHitPoints = false;

  // Equip default weapon
  if (UWorld* World = GetWorld()) {
	FActorSpawnParameters WeaponSpawnParameters;	
    WeaponSpawnParameters.Owner = this;
    WeaponSpawnParameters.Instigator = this;
	auto fRotator = FRotator();

    EquippedWeapon = World->SpawnActor<AWeaponBase>(DefaultWeaponClass,WeaponSpawnParameters);
    if (EquippedWeapon) {
      EquippedWeapon->AttachToComponent(
        PlayerViewCameraComponent,
        FAttachmentTransformRules::SnapToTargetNotIncludingScale,
        FName()
      );
      EquippedWeapon->OnEquip();
	  if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Mamy bro�"));
    }
	else {
		if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("nie ma broni"));
	}
  }

  // Print debug messages
  if (GEngine) {
    GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("PlayerCharacter spawned."));
    FString DebugMessage = FString(TEXT("Player Hit Points: ")).Append(FString::SanitizeFloat(CurrentHitPoints));
    GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, DebugMessage);
  }
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime) {
  Super::Tick(DeltaTime);

  // Regenerate HP
  if (bIsRegeneratingHitPoints) {
    CurrentHitPoints += DeltaTime * HitPointsRegenerationRate;
    if (CurrentHitPoints >= HitPoints) {
      CurrentHitPoints = HitPoints;
      StopRegeneratingHitPoints();
    }
  }
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
  Super::SetupPlayerInputComponent(PlayerInputComponent);

  // Add bindings for walking
  PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
  PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);

  // Add bindings for looking around
  PlayerInputComponent->BindAxis("TurnRight", this, &APlayerCharacter::AddControllerYawInput);
  PlayerInputComponent->BindAxis("LookUp", this, &APlayerCharacter::AddControllerPitchInput);

  // Add bindings for jumping
  PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlayerCharacter::StartJump);
  PlayerInputComponent->BindAction("Jump", IE_Released, this, &APlayerCharacter::StopJump);

  // Add bindings for firing a weapon
  PlayerInputComponent->BindAction("PrimaryFire", IE_Pressed, this, &APlayerCharacter::StartPrimaryFire);
  PlayerInputComponent->BindAction("PrimaryFire", IE_Released, this, &APlayerCharacter::StopPrimaryFire);
  PlayerInputComponent->BindAction("SecondaryFire", IE_Pressed, this, &APlayerCharacter::StartSecondaryFire);
  PlayerInputComponent->BindAction("SecondaryFire", IE_Released, this, &APlayerCharacter::StopSecondaryFire);

  // Add binding for taking damage - for debugging
  FInputActionBinding DebugTakeDamage("DebugTakeDamage", IE_Pressed);
  DebugTakeDamage.ActionDelegate.GetDelegateForManualSet().BindLambda([this](){
    this->TakeDamage(10.0f, FDamageEvent(), nullptr, nullptr);
  });
  PlayerInputComponent->AddActionBinding(DebugTakeDamage);
}

// Moves the character horizontally forwards or backwards relative to the rotation of the controller
void APlayerCharacter::MoveForward(float Value) {
  AddMovementInput(GetActorForwardVector(), Value);
}

// Moves the character horizontally left or right relative to the rotation of the controller
void APlayerCharacter::MoveRight(float Value) {
  AddMovementInput(GetActorRightVector(), Value);
}

// Makes the character jump
void APlayerCharacter::StartJump() {
  bPressedJump = true;
}

// Makes the character stop jumping
void APlayerCharacter::StopJump() {
  bPressedJump = false;
}

// Makes the character start using the currently equipped weapon's primary fire
void APlayerCharacter::StartPrimaryFire() {
  if (EquippedWeapon) {
    EquippedWeapon->OnStartPrimaryFire();
  }

  // Print debug message
  if (GEngine) {
    GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("Player primary fire started."));
  }
}

// Makes the character stop using the currently equipped weapon's primary fire
void APlayerCharacter::StopPrimaryFire() {
  if (EquippedWeapon) {
    EquippedWeapon->OnStopPrimaryFire();
  }
  else if (GEngine) {
	  GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("Chuj, nie ma broni"));
  }

  // Print debug message
  if (GEngine) {
    GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("Player primary fire stopped."));
  }
}

// Makes the character start using the currently equipped weapon's secondary fire
void APlayerCharacter::StartSecondaryFire() {
  if (EquippedWeapon) {
    EquippedWeapon->OnStartSecondaryFire();
  }

  // Print debug message
  if (GEngine) {
    GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("Player secondary fire started."));
  }
}

// Makes the character stop using the currently equipped weapon's secondary fire
void APlayerCharacter::StopSecondaryFire() {
  if (EquippedWeapon) {
    EquippedWeapon->OnStopSecondaryFire();
  }

  // Print debug message
  if (GEngine) {
    GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("Player secondary fire stopped."));
  }
}

// Start regenerating Hit Points
void APlayerCharacter::StartRegeneratingHitPoints() {
  // Print debug message
  if (GEngine) {
    GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("Player health regeneration started."));
  }

  bIsRegeneratingHitPoints = true;
}

// Stop regenerating Hit Points
void APlayerCharacter::StopRegeneratingHitPoints() {
  // Print debug message
  if (GEngine) {
    GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("Player health regeneration stopped."));
  }

  bIsRegeneratingHitPoints = false;
}

// Take damage
float APlayerCharacter::TakeDamage(
  float DamageAmount,
  const FDamageEvent& DamageEvent,
  AController* EventInstigator,
  AActor* DamageCauser
) {
  if (bIsRegeneratingHitPoints) {
    StopRegeneratingHitPoints();
  }

  CurrentHitPoints -= DamageAmount;
  if (CurrentHitPoints <= 0.0f) {
    CurrentHitPoints = 0.0f;
    Die(EventInstigator, DamageCauser);
  } else {
    // Set a timer to start regenerating HP
    GetWorldTimerManager().SetTimer(
      HitPointsRegenerationTimerHandle,
      this,
      &APlayerCharacter::StartRegeneratingHitPoints,
      TimeToRegenerateHitPoints,
      false
    );
  }

  // Print debug message
  if (GEngine) {
    FString DebugMessage = FString(TEXT("Player took ")).Append(FString::SanitizeFloat(DamageAmount))
      .Append(TEXT(" damage, caused by ")).Append(DamageCauser ? DamageCauser->GetName() : TEXT("None"));
    GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, DebugMessage);
    DebugMessage = FString(TEXT("Player Hit Points: ")).Append(FString::SanitizeFloat(CurrentHitPoints));
    GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, DebugMessage);
  }

  return DamageAmount;
}

// Called when player's Hit Points reach 0
// Instigator is the Controller responsible for killing the character
// DeathCauser is the Actor responsible for killing the character
void APlayerCharacter::Die(AController* Instigtor, AActor* DeathCauser) {
  // Turn off health regeneration
  StopRegeneratingHitPoints();
  GetWorldTimerManager().ClearTimer(HitPointsRegenerationTimerHandle);

  // Print debug messages
  if (GEngine) {
    GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Player has died."));
  }

  // Remove the character from the map. To be replaced by something more fancy later
  Destroy();
}
